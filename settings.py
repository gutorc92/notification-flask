"""
Docstring to question history api

route: /history/evaluation
"""
from os import environ
try:
    from dotenv import load_dotenv, find_dotenv
    load_dotenv(find_dotenv())
except EnvironmentError as env_error:
    pass

# Mongo config
MONGO_HOST = environ.get("MONGO_HOST", 'localhost')
MONGO_DBNAME = environ.get("MONGO_DBNAME", 'notifications')
# Mongo config

#Allows regex
MONGO_QUERY_BLACKLIST = ['$where']

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'DELETE', 'PUT']

XML = False
JSON = True
X_DOMAINS = '*'
X_HEADERS = ['Authorization', 'Content-type', 'Cache-Control',
             'If-Match', 'UserEmail']

# pylint: disable=invalid-name
notification_schema = {
    'email': {
        'type': 'string',
        'required': True,
        'regex':r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    },
    'title': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'default': ''},
    'type': {'type': 'string', 'default': 'message', 'allowed': ['message', 'option_question']},
    'status': {'type': 'string', 'default': 'sent', 'allowed': ['sent', 'seen']},
    'options': {'type': 'dict', 'required': False}
}

notification = {
    'schema': notification_schema,
    'item_title': 'notification',
    'resource_methods': ['GET', 'POST'],
}


DOMAIN = {
    'notification': notification,
}
