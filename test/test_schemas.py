import unittest
import os
import json
from api import app
import settings
from base64 import b64encode
from os import environ, path
from eve import Eve
from pymongo.errors import ServerSelectionTimeoutError

try:
    load_dotenv(find_dotenv())
except Exception as e:
    pass


class basicEveTest(unittest.TestCase):
    """Basic Eve test class
    It should let a client available and a app.
    Make requests with:

    self.get('/endpoit')

    """

    def setUp(self):
        dir_path = path.dirname(path.realpath(__file__))
        self.app = app
        self.test_client = self.app.test_client()
        hash = bytes(environ.get("MONGO_USER", '') + ':' +
                     environ.get("MONGO_PASS"), "utf-8")
        self.headers = {
            'Authorization': 'Basic %s' % b64encode(hash).decode("ascii"),
            'Content-Type': 'application/json'
        }
        notification = {
            'email': 'testando@teste.com',
            'title': 'Teste',
            'description': 'Notification test'
        }
        with self.app.app_context():
            self.post('/notification', data=json.dumps(notification))
            response = self.get('/notification?where={"email": "%s"}' % 'testando@teste.com')
            response = json.loads(response.data.decode('utf-8'))
            self.notification = response['_items'][0]

    def tearDown(self):
        self.headers['If-Match'] = self.notification['_etag']
        self.test_client.delete(
            '/notification/{}'.format(self.notification['_id']), headers=self.headers)

    def get(self, url):
        try:
            return self.test_client.get(url, headers=self.headers)
        except ServerSelectionTimeoutError as e:
            self.skipTest('Server TimeOut')

    def post(self, url, data):
        try:
            return self.test_client.post(
                url, headers=self.headers, data=data)
        except ServerSelectionTimeoutError as e:
            self.skipTest('Server TimeOut')

    def delete(self, url, data):
        if '_etag' in data and '_id' in data:
            self.headers['If-Match'] = data['_etag']
            self.test_client.delete(
                '/{0}/{1}'.format(url, data['_id']), headers=self.headers)


class TestNotificationsSchema(basicEveTest):
    """
    Testa a rota de áreas. Verifica se aceita somente os metodos específicados

    coverage run --source=. -m unittest discover -s test
    """
    url = 'notification'

    def test_get(self):
        response = self.get(self.url)
        self.assertEqual(response.status_code, 200)
        response = self.get('/{}/{}'.format(self.url, self.notification['_id']))
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main(verbosity=3)
