"""
Default route:
/history -
    stores objects with the

    user_email: email from user
    qid: question id
    scored: if the user has scored the question at the time
    subject: subject of the question
    time_spent: time pass since user has start doing the question


    see: http://python-eve.org/features.html at Filtering for more details about querys
"""
import logging
import argparse
from os import environ
from dotenv import load_dotenv, find_dotenv
from eve import Eve
from eve.auth import BasicAuth
from flask_cors import CORS
from eve_healthcheck import EveHealthCheck
try:
    load_dotenv(find_dotenv())
except Exception as exception:
    pass


class MyBasicAuth(BasicAuth):
    """ Module to validate the auth user to access the API data"""

    def check_auth(self, username, password, allowed_roles, resource,
                   method):
        return username == environ.get("MONGO_USER") and password == environ.get("MONGO_PASS")


# pylint: disable=invalid-name
app = Eve(auth=MyBasicAuth)
CORS(app)
EveHealthCheck(app, '/healthcheck')
# pylint: enable=invalid-name



app.logger.handlers += logging.getLogger('gunicorn.error').handlers
app.logger.setLevel(logging.INFO)

__formatter__ = logging.Formatter(
    '%(asctime)s %(levelname)s in [%(pathname)s:%(lineno)d]: \n %(message)s\n')

for h in app.logger.handlers:
    h.formatter = __formatter__



ARG_PARSE = argparse.ArgumentParser()

ARG_PARSE.add_argument('-p', '--port', type=int, default=8000, required=False)


if __name__ == '__main__':
    app.logger.setLevel(logging.INFO)
    args = ARG_PARSE.parse_args()
    app.name = 'notifications-service'
    app.run(debug=True, host='0.0.0.0', port=args.port)
